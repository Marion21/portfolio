Contexte du projet

Nous vous demandons de réaliser et mettre en ligne votre portolio.

Il doit avoir au moins les éléments suivants :

Présentation de votre parcours
Présentation de vos compétences et des technologies que vous connaissez
Présentation de vos projets (avec un lien vers un dépôt gitlab et, si possible, une version en ligne)
Vos coordonnées ainsi qu'un formulaire de contact (qui fonctionne).
En bonus, vous pouvez proposer une partie blog.
Votre portfolio doit être attractif et lisible.

Critères pour la revue de code
Lors de la revue de code, nous utiliserons les critères suivants :

Le portfolio est responsive.
L'ecoindex est A ou B.
Le Performance Score est au moins de 80%.
Le site ne comporte aucune erreur d'accessibilité.
Le code est propre est indenté. Tous les critères de performance des compétences C1, C2 et C3 présentées dans le référentiel.


Lien vers le projet en ligne :
http://portfolio-mcoudrais.surge.sh/
